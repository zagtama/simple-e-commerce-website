<!DOCTYPE HTML>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="author" content="Bootstrap-ecommerce by Vosidiy">

<title>UI KIT - Marketplace and Ecommerce html template</title>

<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">

<!-- jQuery -->
<script src="js/jquery-2.0.0.min.js" type="text/javascript"></script>

<!-- Bootstrap4 files-->
<script src="js/bootstrap.bundle.min.js" type="text/javascript"></script>
<link href="css/bootstrap.css" rel="stylesheet" type="text/css"/>

<!-- Font awesome 5 -->
<link href="fonts/fontawesome/css/fontawesome-all.min.css" type="text/css" rel="stylesheet">

<!-- plugin: fancybox  -->
<script src="plugins/fancybox/fancybox.min.js" type="text/javascript"></script>
<link href="plugins/fancybox/fancybox.min.css" type="text/css" rel="stylesheet">

<!-- plugin: owl carousel  -->
<link href="plugins/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
<link href="plugins/owlcarousel/assets/owl.theme.default.css" rel="stylesheet">
<script src="plugins/owlcarousel/owl.carousel.min.js"></script>

<!-- plugin: slickslider -->
<link href="plugins/slickslider/slick.css" rel="stylesheet" type="text/css" />
<link href="plugins/slickslider/slick-theme.css" rel="stylesheet" type="text/css" />
<script src="plugins/slickslider/slick.min.js"></script>

<!-- custom style -->
<link href="css/ui.css" rel="stylesheet" type="text/css"/>
<link href="css/responsive.css" rel="stylesheet" media="only screen and (max-width: 1200px)" />

<!-- custom javascript -->
<script src="js/script.js" type="text/javascript"></script>

<script type="text/javascript">
/// some script

// jquery ready start
$(document).ready(function() {
	// jQuery code

}); 
// jquery end
</script>

</head>
<body>
    <header class="section-header">
        <section class="header-main">
            <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-3">
            <div class="brand-wrap">
                <a href="index.html">
                    <img class="logo" src="images/logo-dark.png" href>
                    <h2 class="logo-text">Ini Logo</h2>
                </a>
            </div> <!-- brand-wrap.// -->
            </div>
            <div class="col-lg-6 col-sm-6">
                <form action="#" class="search-wrap">
                    <div class="input-group">
                        <input type="text" class="form-control" placeholder="Search">
                        <div class="input-group-append">
                          <button class="btn btn-primary" type="submit">
                            <i class="fa fa-search"></i>
                          </button>
                        </div>
                    </div>
                </form> <!-- search-wrap .end// -->
            </div> <!-- col.// -->
            <div class="col-lg-3 col-sm-6">
                <div class="widgets-wrap d-flex justify-content-end">
                    <div class="widget-header">
                        <a href="cart.php" class="icontext">
                            <div class="icon-wrap icon-xs bg2 round text-secondary"><i class="fa fa-shopping-cart"></i></div>
                            <div class="text-wrap">
                                <small>Keranjang</small>
                                <span>3 items</span>
                            </div>
                        </a>
                    </div> <!-- widget .// -->
                    <div class="widget-header dropdown">
                        <a href="#" class="ml-3 icontext" data-toggle="dropdown" data-offset="20,10">
                            <div class="icon-wrap icon-xs bg2 round text-secondary"><i class="fa fa-user"></i></div>
                            <div class="text-wrap">
                                <small>Hello.</small>
                                <span>Login <i class="fa fa-caret-down"></i></span>
                            </div>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <form class="px-4 py-3">
                                <div class="form-group">
                                  <label>Email address</label>
                                  <input type="email" class="form-control" placeholder="email@example.com">
                                </div>
                                <div class="form-group">
                                  <label>Password</label>
                                  <input type="password" class="form-control" placeholder="Password">
                                </div>
                                <button type="submit" class="btn btn-primary">Sign in</button>
                                </form>
                                <hr class="dropdown-divider">
                                <a class="dropdown-item" href="#">Have account? Sign up</a>
                                <a class="dropdown-item" href="#">Forgot password?</a>
                        </div> <!--  dropdown-menu .// -->
                    </div> <!-- widget  dropdown.// -->
                </div>	<!-- widgets-wrap.// -->	
            </div> <!-- col.// -->
        </div> <!-- row.// -->
            </div> <!-- container.// -->
        </section> <!-- header-main .// -->
        
        
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark white">
          <div class="container">
        
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main_nav" aria-controls="main_nav" aria-expanded="false" aria-label="Toggle navigation">
              <span class="navbar-toggler-icon"></span>
            </button>
        
            <div class="collapse navbar-collapse" id="main_nav">
              <ul class="navbar-nav">
                <a class="nav-link pl-0" href="index.php"> <strong>Semua Kategori</strong></a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="makeup.php">Makeup</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="skincare.php">Skincare</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="outfit.php">Outfit</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" href="hijab.php">Hijab</a>
                </li>
              </ul>
            </div> <!-- collapse .// -->
          </div> <!-- container .// -->
        </nav>
        
        </header> <!-- section-header.// -->
        

<!-- ========================= SECTION CONTENT ========================= -->
<section class="section-content bg padding-y">
  <div class="container">
    <div class="row">
  <article class="card card-product">
    <div class="card-body">
    <div class="row">
      <aside class="col-sm-3">
        <div class="img-wrap"><img src="images/skincare2.jpg"></div>
      </aside> <!-- col.// -->
      <article class="col-sm-6">
          <h4 class="title">NACIFIC Natural Pacific Fresh Herb Origin Serum 50ml</h4>
          <div class="rating-wrap  mb-2">
            <ul class="rating-stars">
              <li style="width:80%" class="stars-active"> 
                <i class="fa fa-star"></i> <i class="fa fa-star"></i> 
                <i class="fa fa-star"></i> <i class="fa fa-star"></i> 
                <i class="fa fa-star"></i> 
              </li>
              <li>
                <i class="fa fa-star"></i> <i class="fa fa-star"></i> 
                <i class="fa fa-star"></i> <i class="fa fa-star"></i> 
                <i class="fa fa-star"></i> 
              </li>
            </ul>
            <div class="label-rating">132 reviews</div>
            <div class="label-rating">154 orders </div>
          </div> <!-- rating-wrap.// -->
          <p> NEW PACKAGING YAH Natural Pacific per 16 April 2018 berganti nama brand jadi NACIFIC Isi & kandungan serum di dalamnya masih sama, hanya di nama brand & packaging saja Dapatkan hasil maksimal dari pemakaian Nacific: Pagi: gunakan Pytho Niacin Whitening Essence Malam: gunakan Fresh Herb Origin Serum ORIGINAL Made in Korea 100% yah Serum ini di korea lebih dikenal dengan nama serum "Jun Ji Hyun". Serum ini diperkaya dengan anti oksidan yang meningkatkan elastisitas, mencegah kerutan dini, menyeimbangkan minyak, kelembaban dan nada pada wajah. Buat yang belum pernah pakai serum ini, jangan kaget ya ketika pertama kali digunakan, mungkin akan ada rasa efek kesemutan pada saat dioleskan pada kulit yang rusak (seperti ruam ruam, merah dan radang karena jerawat), semakin parah kondisi kulit, semakin gatal dan kesemutan. Terapi, seiring dengan teratur nya pemakaian, gatal2 dan kesemutan berkurang serta kulit cerah dan elastis menanti! Recommended banget yang mempunyai kondisi kulit seperti: - kulit dengan pori pori besar - kulit yang menderita keriput halus - memiliki banyak masalah kulit - kulit yang cepat mengering - kulit kasar Cara penggunaan: - bersihkan wajah, gunakan pada pagi dan sore hari, aplikasikan toner, lalu serum. Pastikan di kocok dahulu botol serum sebelum penggunaan - gunakan spoid / pipet dan oleskan serum ke wajah dengan perlahan dan menyebar- oleskan krim</p>
           <dl class="dlist-align">
            <dt>Merek</dt>
            <dd>Nacific</dd>
          </dl>  <!-- item-property-hor .// -->
          <dl class="dlist-align">
            <dt>Model</dt>
            <dd>Serum</dd>
          </dl>  <!-- item-property-hor .// -->
          <dl class="dlist-align">
            <dt>Tipe</dt>
            <dd>Serum Wajah</dd>
          </dl>  <!-- item-property-hor .// -->
        
      </article> <!-- col.// -->
      <aside class="col-sm-3 border-left">
        <div class="action-wrap">
          <div class="price-wrap h4">
            <span class="price"> Rp.56 </span>	
            <del class="price-old"> Rp.98</del>
          </div> <!-- info-price-detail // -->
          <p class="text-success">Gratis Ongkir</p>
          <br>
          <p>
            <a href="#" class="btn btn-primary"> Tambah ke keranjang </a>
            <a href="#" class="btn btn-secondary"> Details  </a>
          </p>
          <a href="#"><i class="fa fa-heart"></i> Tambah ke wishlist</a>
        </div> <!-- action-wrap.// -->
      </aside> <!-- col.// -->
    </div> <!-- row.// -->
    </div> <!-- card-body .// -->
  </article> <!-- card product .// -->
  
  <article class="card card-product">
    <div class="card-body">
    <div class="row">
      <aside class="col-sm-3">
        <div class="img-wrap"><img src="images/skincare1.jpg"></div>
      </aside> <!-- col.// -->
      <article class="col-sm-6">
          <h4 class="title">NATURE REPUBLIC ALOE VERA SHOOTING GEL 300 ML 100% ORIGINAL</h4>
          <div class="rating-wrap mb-2">
            <ul class="rating-stars">
              <li style="width:50%" class="stars-active"> 
                <i class="fa fa-star"></i> <i class="fa fa-star"></i> 
                <i class="fa fa-star"></i> <i class="fa fa-star"></i> 
                <i class="fa fa-star"></i> 
              </li>
              <li>
                <i class="fa fa-star"></i> <i class="fa fa-star"></i> 
                <i class="fa fa-star"></i> <i class="fa fa-star"></i> 
                <i class="fa fa-star"></i> 
              </li>
            </ul>
            <div class="label-rating">12 reviews</div>
            <div class="label-rating">154 orders </div>
          </div> <!-- rating-wrap.// -->
          <p>NATURE REPUBLIC SOOTHING & MOISTURE ALOE VERA 92% SOOTHING GEL 300 ML- READY STOK- 100% ORIGINAL dari KOREA- Volume: 300 ml, tanggal expiry : tahun 2020- Mengandung 92% ekstrak lidah buaya.- Fresh, wangi dan bebas pewarna buatan.- Bisa digunakan untuk rambut, wajah, badan, dan kuku.MANFAAT PRODUK INI:1. Memberikan nutrisi yang sangat baik untuk kulit wajah dan tubuh.2. Menghaluskan, melembabkan, menenangkan, menyegarkan, mencerahkan, dan melembutkan kulit.BISA DIGUNAKAKAN UNTUK:1. Masker rambut2. Masker wajah3. Alas dasar make-up4. Body lotion5. Night cream6. Perawatan kuku7. Mengatasi kulit yang terbakar karena panas matahari</p>
          <dl class="dlist-align">
            <dt>Merek</dt>
            <dd>Nature Republic</dd>
          </dl>  <!-- item-property-hor .// -->
          <dl class="dlist-align">
            <dt>Model</dt>
            <dd>Pelembab</dd>
          </dl>  <!-- item-property-hor .// -->
          <dl class="dlist-align">
            <dt>Tipe</dt>
            <dd>Gel</dd>
          </dl>  <!-- item-property-hor .// -->
      </article> <!-- col.// -->
      <aside class="col-sm-3 border-left">
        <div class="action-wrap">
          <div class="price-wrap h4">
            <span class="price"> Rp.245 </span>	
            <del class="price-old"> Rp.528</del>
          </div> <!-- info-price-detail // -->
          <p class="text-success">Gratis Ongkir</p>
          <br>
          <p>
            <a href="#" class="btn btn-primary"> Tambah ke keranjang </a>
            <a href="#" class="btn btn-secondary"> Details  </a>
          </p>
          <a href="#"><i class="fa fa-heart"></i> Tambah ke wishlist</a>
        </div> <!-- action-wrap.// -->
      </aside> <!-- col.// -->
    </div> <!-- row.// -->
    </div> <!-- card-body .// -->
  </article> <!-- card product .// -->
  
    </main> <!-- col.// -->
  </div>
  
  </div> <!-- container .//  -->
  </section>
  <!-- ========================= SECTION CONTENT END// ========================= -->
  

<!-- ========================= FOOTER ========================= -->
<footer class="section-footer bg-dark">
	<div class="container">
		<section class="footer-top padding-top">
			<div class="row">
				<aside class="col-sm-3 col-md-3 white">
                <h5 class="title">Kategori</h5>
					<ul class="list-unstyled">
						<li> <a href="makeup.php">Makeup</a></li>
						<li> <a href="skincare.php">Skincare</a></li>
						<li> <a href="outfit.php">Outfit</a></li>
						<li> <a href="hijab.php">Hijab</a></li>
					</ul>
				</aside>
				<aside class="col-sm-3  col-md-3 white">
				</aside>
				<aside class="col-sm-3  col-md-3 white">
				</aside>
				<aside class="col-sm-3">
					<article class="white">
						<h5 class="title">Contacts</h5>
						<p>
							<strong>Phone: </strong> +123456789 <br> 
						    <strong>Fax:</strong> +123456789
						</p>

						 <div class="btn-group white">
						    <a class="btn btn-facebook" title="Facebook" target="_blank" href="#"><i class="fab fa-facebook-f  fa-fw"></i></a>
						    <a class="btn btn-instagram" title="Instagram" target="_blank" href="#"><i class="fab fa-instagram  fa-fw"></i></a>
						    <a class="btn btn-youtube" title="Youtube" target="_blank" href="#"><i class="fab fa-youtube  fa-fw"></i></a>
						    <a class="btn btn-twitter" title="Twitter" target="_blank" href="#"><i class="fab fa-twitter  fa-fw"></i></a>
						</div>
					</article>
				</aside>
			</div> <!-- row.// -->
			<br> 
		</section>
	</div><!-- //container -->
</footer>
<!-- ========================= FOOTER END // ========================= -->



</body>
</html>